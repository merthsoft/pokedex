Pokedex 84+CE
2016 Merthsoft Creations
shaun@shaunmcfall.com

Pokedex for the TI-84+CE. 

Requires the CE libraries made by MateoConLechuga:
https://github.com/CE-Programming/libraries/releases/

Source code:
The source is maintained in a git repository available here: https://bitbucket.org/merthsoft/pokedex/

Credits:
Many thanks to Mateo for the CE toolchain, libraries, and CEmu, all of which made this possible. He also fixed
many bugs for me I found along the way.

Thanks to Tari for all his programming help. I'm bad at C, and he is significantly better :)

Data from http://veekun.com

Revision History:
