using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class ItemName {
        ushort item_id { get; set; }
        int local_language_id { get; set; }
        string name { get; set; }
    }
}
