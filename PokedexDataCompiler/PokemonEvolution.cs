using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokemonEvolution {
        ushort evolved_species_id { get; set; }
        string evolution_trigger_id { get; set; }
        string trigger_item_id { get; set; }
        string minimum_level { get; set; }
        string gender_id { get; set; }
        string location_id { get; set; }
        string held_item_id { get; set; }
        string time_of_day { get; set; }
        string known_move_id { get; set; }
        string known_move_type_id { get; set; }
        string minimum_happiness { get; set; }
        string minimum_beauty { get; set; }
        string minimum_affection { get; set; }
        string relative_physical_stats { get; set; }
        string party_species_id { get; set; }
        string party_type_id { get; set; }
        string trade_species_id { get; set; }
        string needs_overworld_rain { get; set; }
        string turn_upside_down { get; set; }
    }
}
