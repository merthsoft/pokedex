using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokemonDescriptionEntry {
        public int species_id { get; set; }
        public int version_id { get; set; }
        public int language_id { get; set; }
        public string flavor_text { get; set; }
    }
}
