using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class AbilityName {
        public int ability_id { get; set; }
        public int local_language_id { get; set; }
        public string name { get; set; }
    }
}
