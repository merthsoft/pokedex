using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokedexEntry {
        public int id { get; set; }
        public string identifier { get; set; }
        public short species_id { get; set; }
        public ushort height { get; set; }
        public ushort weight { get; set; }
        public short base_experience { get; set; }
        public int order { get; set; }
        public bool is_default { get; set; }
    }
}
