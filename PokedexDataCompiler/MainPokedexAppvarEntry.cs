using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace PokedexDataCompiler {
    class MainPokedexAppvarEntry {
        public short Id { get; set; }
        public bool IsDefault { get; set; }
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string Genus { get; set; }
        public PokemonDataAppvarEntry DataEntry { get; set; }
        public Bitmap Sprite { get; set; }
        public Bitmap Icon { get; set; }
    }
}
