using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokedexSpeciesNameEntry {
        public int pokemon_species_id { get; set; }
        public int local_language_id { get; set; }
        public string name { get; set; }
        public string genus { get; set; }
    }
}
