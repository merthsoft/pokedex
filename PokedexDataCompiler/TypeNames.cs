using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class TypeName {
        public short type_id { get; set; }
        public int local_language_id { get; set; }
        public string name { get; set; }
    }
}
