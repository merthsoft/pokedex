﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Merthsoft.Extensions {
	public static class HexHelper {
		public static bool IsHexString(string s) {
			return s.StartsWith("0x") || s.StartsWith("$") || s.EndsWith("h");
		}

		public static int GetInt(string s) {
			byte[] arr = GetByteArray(s);
			return BitConverter.ToInt32(arr, 0);
		}

		/// <summary>
		/// Converts the passed in string of hex characters to an array of bytes.
		/// </summary>
		/// <param name="s">The string to convert. Must be a hex string, can be prefaced with "0x" or "$", or end with "h".</param>
		/// <param name="arrayLen">The desired length of the returned array, will zero fill the end of the array.</param>
		/// <returns>An array of bytes corresponding to the hex string passed in.</returns>
		/// <example>GetByteArray("0xFFFF") => {0xFF, 0xFF}</example>
		public static byte[] GetByteArray(string s, int arrayLen = -1) {
			if (s.StartsWith("0x"))
				s = s.Remove(0, 2);
			if (s.StartsWith("$"))
				s = s.Remove(0, 1);
			if (s.EndsWith("h"))
				s = s.Remove(s.Length - 1);
			int n = s.Length;
			byte[] bytes;
			if (arrayLen == -1)
				bytes = new byte[n / 2];
			else
				bytes = new byte[arrayLen];
			for (int i = 0; i < n; i += 2) {
				try {
					bytes[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
				} catch {
					//failed to convert these 2 chars, they may contain illegal characters
					bytes[i / 2] = 0;
				}
			}
			return bytes;
		}

		/// <summary>
		/// Takes a hex string and converts it into a two dimensional array of 1s and 0s.
		/// </summary>
		/// <param name="hex">The hex string to convert.</param>
		/// <param name="w">The width of the array.</param>
		/// <param name="h">The height of the array.</param>
		/// <returns>A w-by-h array of ones and zeros.</returns>
		public static int[,] HexBinToArr(string hex, int w, int h) {
			return BinToArr(HexToBin(hex), w, h);
		}

		public static List<List<int>> HexTo2DList(string hex, int w, out int h, int bitsPerPixel) {
			List<int> arr = HexToList(hex, w, bitsPerPixel, out h);
			return arr.To2DList(w, h);
		}

		public static int[,] HexToArr(string hex, int w, out int h, int bitsPerPixel) {
			List<int> arr = HexToList(hex, w, bitsPerPixel, out h);
			return arr.ToArray(w, h);
		}

		private static List<int> HexToList(string hex, int w, int bitsPerPixel, out int h) {
			List<int> arr = new List<int>();
			int stringIndex = 0;
			int stringStep = bitsPerPixel / 4 == 0 ? 1 : bitsPerPixel / 4;
			h = 0;
			int length = hex.Length;

			while (stringIndex < length) {
				for (int i = 0; i < w; i++) {
					string c = hex.Substring(stringIndex, stringStep).ToUpper();
					stringIndex += stringStep;
					if (c[0] == 'G') {
						stringIndex -= stringStep;
						stringIndex++;
						while (i < w) {
							arr.Add(0);
							i++;
						}
						break;
					}
					if (c == "H") {
						c = "0";
					}
					if (bitsPerPixel == 1) {
						string bin = HexToBin(c);
						for (int k = 0; k < 4; k++) {
							//arr[i + k, j] = bin[k] - '0';
							arr.Add(bin[k] - '0');
						}
						i += 3;
					} else {
						int val = Convert.ToInt32(c, 16);
						//arr[i, j] = val;
						arr.Add(val);
					}
				}
				h++;
			}

			return arr;
		}

		/// <summary>
		/// Takes a binary string and converts it into a two dimensional array of 1s and 0s.
		/// </summary>
		/// <param name="bin">The binary string to convert.</param>
		/// <param name="w">The width of the array.</param>
		/// <param name="h">The height of the array.</param>
		/// <returns>A w-by-h array of ones and zeros.</returns>
		public static int[,] BinToArr(string bin, int w, int h) {
			int a = 0;
			int[,] arr = new int[w, h];
			for (int j = 0; j < h; j++) {
				for (int i = 0; i < w; i++) {
					if (a < bin.Length) {
						if (bin[a++] == 'X') { break; }
						arr[i, j] = int.Parse(bin[a-1].ToString());
					}
				}
			}

			return arr;
		}

		/// <summary>
		/// Converts a string of binary to a string of hex.
		/// </summary>
		/// <param name="bin">The binary string to convert.</param>
		/// <returns>A string that has the hex conversion of the given binary string.</returns>
		public static string BinToHex(string bin) {
			string ret = "";
			while (bin != "") {
				string b;
				if (bin.Length >= 4) {
					b = bin.Substring(0, 4);
					bin = bin.Remove(0, 4);
				} else {
					b = bin;
					bin = "";
				}
				ret += String.Format("{0:X}", Convert.ToInt16(b, 2));
			}

			return ret;
		}

		/// <summary>
		/// Converts a hex string to a binary string.
		/// </summary>
		/// <param name="hex">The hex string to convert.</param>
		/// <returns>A string that has the binary conversion of the given hex string.</returns>
		/// <exception cref="FormatException"></exception>
		public static string HexToBin(string hex) {
			StringBuilder ret = new StringBuilder(hex.Length * 4);
			foreach (char h in hex.ToUpper()) {
				switch (h) {
					case 'H':
					case '0':
						ret.Append("0000");
						break;
					case '1':
						ret.Append("0001");
						break;
					case '2':
						ret.Append("0010");
						break;
					case '3':
						ret.Append("0011");
						break;
					case '4':
						ret.Append("0100");
						break;
					case '5':
						ret.Append("0101");
						break;
					case '6':
						ret.Append("0110");
						break;
					case '7':
						ret.Append("0111");
						break;
					case '8':
						ret.Append("1000");
						break;
					case '9':
						ret.Append("1001");
						break;
					case 'A':
						ret.Append("1010");
						break;
					case 'B':
						ret.Append("1011");
						break;
					case 'C':
						ret.Append("1100");
						break;
					case 'D':
						ret.Append("1101");
						break;
					case 'E':
						ret.Append("1110");
						break;
					case 'F':
						ret.Append("1111");
						break;
					case 'G':
						ret.Append("X");
						break;
					default:
						throw new FormatException("Hex string not properly formatted.");
				}
			}

			return ret.ToString();
		}
	}
}
