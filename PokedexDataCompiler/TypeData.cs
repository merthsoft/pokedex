using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class TypeData {
        public short id { get; set; }
        public string identifier { get; set; }
    }
}
