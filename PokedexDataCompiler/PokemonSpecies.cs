using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokemonSpecies {
        ushort id { get; set; }
        string identifier { get; set; }
        string generation_id { get; set; }
        string evolves_from_species_id { get; set; }
        string evolution_chain_id { get; set; }
        string color_id { get; set; }
        string shape_id { get; set; }
        string habitat_id { get; set; }
        string gender_rate { get; set; }
        string capture_rate { get; set; }
        string base_happiness { get; set; }
        string is_baby { get; set; }
        string hatch_counter { get; set; }
        string has_gender_differences { get; set; }
        string growth_rate_id { get; set; }
        string forms_switchable { get; set; }
        string order { get; set; }
        string conquest_order { get; set; }
    }
}
