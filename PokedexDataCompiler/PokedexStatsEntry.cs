using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokedexStatsEntry {
        public int pokemon_id { get; set; }
        public int stat_id { get; set; }
        public short base_stat { get; set; }
        public int effort { get; set; }
    }
}
