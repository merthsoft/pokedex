using CsvHelper;
using libWiiSharp;
using Merthsoft.CalcData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PokedexDataCompiler {
    class PokemonAbility {
        public int pokemon_id { get; set; }
        public short ability_id { get; set; }
        public int is_hidden { get; set; }
        public int slot { get; set; }
    }
}
