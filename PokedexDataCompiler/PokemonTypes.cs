using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokemonType {
        public short pokemon_id { get; set; }
        public byte type_id { get; set; }
        public byte slot { get; set; }
    }
}
