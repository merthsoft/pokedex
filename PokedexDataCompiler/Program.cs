﻿using CsvHelper;
using libWiiSharp;
using Merthsoft.CalcData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PokedexDataCompiler {
    class Program {
        private static readonly Encoding encoding = Encoding.GetEncoding(437);
        private static readonly List<Color> palette = new List<Color>(256);

        static void Main(string[] args) {
            const ushort MAX_APPVAR_SIZE = ushort.MaxValue - 34;

            List<MainPokedexAppvarEntry> mainData = new List<MainPokedexAppvarEntry>();

            for (int i = 0; i < 256; i++) {
                Color color = MerthsoftExtensions.ColorFrom8BitHLRGB(i);
                palette.Add(color);
            }

            var pokedexRecords = readFile<PokedexEntry>("data\\pokemon.csv").Where(s => s.is_default || s.identifier.Contains("-mega"));
            var pokemonStats = readFile<PokedexStatsEntry>("data\\pokemon_stats.csv");
            var pokemonDescriptions = readFile<PokemonDescriptionEntry>("data\\pokemon_species_flavor_text.csv").Where(s => s.language_id == 9);
            var pokemonNames = readFile<PokedexSpeciesNameEntry>("data\\pokemon_species_names.csv").Where(s => s.local_language_id == 9);
            var pokemonTypes = readFile<PokemonType>("data\\pokemon_types.csv");
            var typeNames = readFile<TypeName>("data\\type_names.csv").Where(s => s.local_language_id == 9);
            var typeData = readFile<TypeData>("data\\types.csv");
            var abilities = readFile<PokemonAbility>("data\\pokemon_abilities.csv");
            var abilityNames = readFile<AbilityName>("data\\ability_names.csv").Where(s=>s.local_language_id == 9).ToList();
            var itemNames = readFile<ItemName>("data\\item_names.csv");
            var species = readFile<PokemonSpecies>("data\\pokemon_species.csv");
            var evolutionData = readFile<PokemonEvolution>("data\\pokemon_evolution.csv");

            foreach (var record in pokedexRecords.OrderBy(s => s.order)) {
                var entry = new MainPokedexAppvarEntry() {
                    Id = record.species_id,
                    IsDefault = record.is_default,
                    Identifier = record.identifier,
                    Name = pokemonNames.First(f => f.pokemon_species_id == record.species_id).name,
                    Genus = pokemonNames.First(f => f.pokemon_species_id == record.species_id).genus,
                };

                string fileName = "";
                if (!entry.IsDefault) {
                    fileName = string.Format("icons\\{0}-{1}.png", entry.Id, string.Join("-", entry.Identifier.Split('-').Skip(1)));
                }

                if (entry.IsDefault || fileName == "" || !File.Exists(fileName)) {
                    fileName = string.Format("icons\\{0}.png", entry.Id);
                }

                entry.Icon = CropBitmap(fileName);
                //entry.Sprite = CropBitmap(fileName.Replace("icons\\", "sprites\\"));

                var statsRecords = pokemonStats.Where(s => s.pokemon_id == entry.Id).OrderBy(s => s.stat_id).ToList();

                entry.DataEntry = new PokemonDataAppvarEntry() {
                    Height = record.height,
                    Weight = record.weight,
                    BaseExperience = record.base_experience,
                    Hp = (byte)statsRecords[0].base_stat,
                    Attack = (byte)statsRecords[1].base_stat,
                    Defense = (byte)statsRecords[2].base_stat,
                    SpecialAttack = (byte)statsRecords[3].base_stat,
                    SpecialDefense = (byte)statsRecords[4].base_stat,
                    Speed = (byte)statsRecords[5].base_stat,
                    Description = pokemonDescriptions.Where(s => s.species_id == entry.Id).OrderByDescending(s => s.version_id).First().flavor_text,
                    Type1 = pokemonTypes.First(s => s.pokemon_id == entry.Id && s.slot == 1).type_id,
                    // Visual studio doesn't like this line, and that annoys me.
                    //Type2 = pokemonTypes.FirstOrDefault(s => s.pokemon_id == entry.Id && s.slot == 2)?.type_id ?? 0,
                    Abilities = abilities.Where(a => a.pokemon_id == entry.Id).OrderBy(a => a.slot).Select(a => a.ability_id).ToList(),

                };
                var type2 = pokemonTypes.FirstOrDefault(s => s.pokemon_id == entry.Id && s.slot == 2);
                if (type2 != null) {
                    entry.DataEntry.Type2 = type2.type_id;
                }

                mainData.Add(entry);
            }

            List<byte> mainDataArray = new List<byte>();
            List<List<byte>> statsDataArrays = new List<List<byte>>();

            statsDataArrays.Add(new List<byte>());
            byte statsDataArrayIndex = 0;

            mainDataArray.AddRange(((ushort)mainData.Count(s => s.IsDefault)).ToByteArray());

            mainDataArray.AddRange(getImageData(CropBitmap("logo.png")));

            mainDataArray.AddRange(getImageData(CropBitmap("pokeball_cursor.png")));
            
            foreach (var entry in mainData.OrderBy(s => s.Id)) {
                var tempArray = getDataArray(entry);

                if (statsDataArrays[statsDataArrayIndex].Count + tempArray.Length > MAX_APPVAR_SIZE) {
                    statsDataArrayIndex++;
                    statsDataArrays.Add(new List<byte>());
                }

                statsDataArrays[statsDataArrayIndex].AddRange(tempArray);

                mainDataArray.AddRange(entry.Id.ToByteArray());
                mainDataArray.Add((byte)(entry.IsDefault ? 1 : 0));

                mainDataArray.Add(statsDataArrayIndex);
                mainDataArray.AddRange((((ushort)(statsDataArrays[statsDataArrayIndex].Count- tempArray.Length)).ToByteArray()));
            }
                        
            List<byte> typeDataArray = new List<byte>();
            typeDataArray.Add((byte)typeData.Count);
            foreach(var type in typeData) {
                string typeName = typeNames.First(t => t.type_id == type.id).name;
                typeDataArray.Add((byte)type.id);
                typeDataArray.AddRange(getStringData(typeName));
                typeDataArray.AddRange(getImageData(CropBitmap(string.Format("types\\{0}.png", type.identifier))));
            }
            

            List<byte> abilityDataArray = new List<byte>();
            foreach (var ability in abilityNames) {
                abilityDataArray.AddRange(((short)ability.ability_id).ToByteArray());
                abilityDataArray.AddRange(getStringData(ability.name));
            }

            saveAppVar(mainDataArray.Concat(typeDataArray).Concat(abilityDataArray), "[PKDXM");
            //saveAppVar(mainDataArray, "[PKDXM");
            //saveAppVar(typeDataArray, "[PKDXT");
            //saveAppVar(abilityDataArray, "[PKDXA");

            for (statsDataArrayIndex = 0; statsDataArrayIndex < statsDataArrays.Count; statsDataArrayIndex++) {
                saveAppVar(statsDataArrays[statsDataArrayIndex], "[PKDXD" + statsDataArrayIndex);
            }

            Console.WriteLine("Max icon width: {0}", mainData.Max(s => s.Icon.Width));
            Console.WriteLine("Max icon height: {0}", mainData.Max(s => s.Icon.Height));
            Console.WriteLine();
            Console.WriteLine("Length of main data: {0}", mainDataArray.Count);
            Console.ReadLine();
        }

        private static List<byte> getStringData(string str) {
            List<byte> typeDataArray = new List<byte>();
            str = RemoveDiacritics(Regex.Replace(str, @"\s+", " "));
            typeDataArray.Add((byte)encoding.GetByteCount(str));
            typeDataArray.AddRange(encoding.GetBytes(str));
            return typeDataArray;
        }

        static string RemoveDiacritics(string text) {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString) {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark) {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        private static List<byte> getImageData(Bitmap image) {
            var encoder = new Lz77();
            List<byte> data = new List<byte>();
            data.Add((byte)image.Width);
            data.Add((byte)image.Height);

            int spriteSize = image.Height * image.Width;
            int[,] spriteData = image.PalettizeImage(palette, 255);

            List<byte> rawImage = new List<byte>();
            for (int j = 0; j < image.Height; j++) {
                for (int i = 0; i < image.Width; i++) {
                    rawImage.Add((byte)spriteData[i, j]);
                }
            }

            data.AddRange(rawImage);
            return data;
        }

        private static byte[] getDataArray(MainPokedexAppvarEntry entry) {
            List<byte> tempArray = new List<byte>();
            var statsEntry = entry.DataEntry;

            tempArray.AddRange(getStringData(entry.Name));
            tempArray.AddRange(getStringData(entry.Genus));

            tempArray.AddRange(getImageData(entry.Icon));

            tempArray.AddRange(getStringData(entry.DataEntry.Description));

            tempArray.Add(statsEntry.Type1);
            tempArray.Add(statsEntry.Type2);

            tempArray.AddRange(statsEntry.Height.ToByteArray());
            tempArray.AddRange(statsEntry.Weight.ToByteArray());
            tempArray.Add(statsEntry.Hp);
            tempArray.Add(statsEntry.Attack);
            tempArray.Add(statsEntry.Defense);
            tempArray.Add(statsEntry.SpecialAttack);
            tempArray.Add(statsEntry.SpecialDefense);
            tempArray.Add(statsEntry.Speed);

            for (int i = 0; i < 3; i++) {
                short value = 0;
                if (statsEntry.Abilities.Count > i) {
                    value = statsEntry.Abilities[i];
                }
                tempArray.AddRange(value.ToByteArray());
            }

            tempArray.Add(0x00); // Evolution vector appvar index
            tempArray.Add(0x00); // Evolution vector location
            tempArray.Add(0x00); // Evolution vector location
            
            return tempArray.ToArray();
        }

        private static void saveAppVar(IEnumerable<byte> mainDataArray, string name) {
            AppVar8x mainDataAppVar = new AppVar8x();
            mainDataAppVar.SetRawData(mainDataArray.ToArray());
            mainDataAppVar.Name = name;
            mainDataAppVar.Archived = true;
            using (FileStream f = File.Open(name + ".8xv", FileMode.Create))
            using (BinaryWriter bw = new BinaryWriter(f)) {
                mainDataAppVar.Save(bw);
            }
        }

        static List<T> readFile<T>(string file) {
            using (var reader = File.OpenText(file)) {
                var pokedex = new CsvReader(reader);
                return pokedex.GetRecords<T>().ToList();
            }
        }

        public static Bitmap CropBitmap(string fileName) {
            Bitmap bmp = (Bitmap)Bitmap.FromFile(fileName);
            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).A != 0)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).A != 0)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row) {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row) {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col) {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col) {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target)) {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                target.Save(fileName + ".crop.png");
                return target;
            } catch (Exception ex) {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }
    }
}
