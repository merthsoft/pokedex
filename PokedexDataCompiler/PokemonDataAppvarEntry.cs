using System;
using System.Collections.Generic;
using System.Linq;

namespace PokedexDataCompiler {
    class PokemonDataAppvarEntry {
        public ushort Height { get; set; }
        public ushort Weight { get; set; }
        public byte Hp { get; set; }
        public byte Attack { get; set; }
        public byte Defense { get; set; }
        public byte SpecialAttack { get; set; }
        public byte SpecialDefense { get; set; }
        public byte Speed { get; set; }
        public byte Accuracy { get; set; }
        public byte Evasion { get; set; }

        public short BaseExperience { get; set; }
        public string Description { get; set; }
        public List<short> EvolvesTo { get; set; }
        public List<short> EvolutionType { get; set; }
        public List<short> EvolutionData { get; set; }
        public List<short> Abilities { get; set; }

        public ushort AppVarLocation { get; set; }

        public byte Type1 { get; set; }
        public byte Type2 { get; set; }
    }
}
