﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Xml;

//namespace Merthsoft.Extensions {
/// <summary>
/// Extensions.
/// </summary>
public static class MerthsoftExtensions {
    public static bool IsControlDown { get { return (Control.ModifierKeys & Keys.Control) == Keys.Control; } }
    public static bool IsAltDown { get { return (Control.ModifierKeys & Keys.Alt) == Keys.Alt; } }
    public static bool IsShiftDown { get { return (Control.ModifierKeys & Keys.Shift) == Keys.Shift; } }

    public static bool IsRunningOnMono() {
        return Type.GetType("Mono.Runtime") != null;
    }

    /// <summary>
    /// Returns the element at a specified index in a sequence or a default value
    /// if the index is out of range. Then attempts to run the converter on it. 
    /// Ignores any exceptions that happen in the converter.
    /// </summary>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <param name="list">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
    /// <param name="index">The zero-based index of the element to retrieve.</param>
    /// <param name="converter">The function to apply to the value from the list.</param>
    /// <returns>
    /// default(TSource) if the index is outside the bounds of the source sequence;
    /// otherwise, the element at the specified position in the source sequence.
    /// </returns>
    public static T ElementAtOrDefault<T>(this IEnumerable<T> list, int index, Func<T, T> converter) {
        T ret = list.ElementAtOrDefault(index);
        try {
            ret = converter(ret);
        } catch { }

        return ret;
    }

    /// <summary>
    /// Checks if a substring exists at a specific location within a string.
    /// </summary>
    /// <param name="s">The string to check in.</param>
    /// <param name="value">The substring to look for.</param>
    /// <param name="location">The location within the string to check in.</param>
    /// <returns>True if the substring exists at the location within the string.
    /// False otherwise.</returns>
    public static bool AtLocation(this char[] s, string value, int location) {
        if (location + value.Length > s.Length) { return false; }
        for (int i = 0; i < value.Length; i++) {
            if (s[i + location] != value[i]) {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Makes replacements within a string based on the keys in the passed in dictionary.
    /// Replaces with the corresponding value.
    /// </summary>
    /// <param name="oldString">The string to replace in.</param>
    /// <param name="replacements">The replacements to be made.</param>
    /// <param name="newString">The new string created after the replacements.</param>
    /// <returns>The locations within the original string the replacements were made,
    /// and the old value that was replaced.</returns>
    public static List<Tuple<int, string>> Replace(this string oldString, Dictionary<string, string> replacements, out string newString) {
        var ret = new List<Tuple<int, string>>();
        StringBuilder sb = new StringBuilder(oldString.Length * 5);
        char[] old = oldString.ToCharArray();

        if (replacements.Count == 0) {
            newString = oldString;
            return ret;
        }

        int oldLocation = 0;
        var nonNull = replacements.Where(r => !string.IsNullOrWhiteSpace(r.Value) && !string.IsNullOrWhiteSpace(r.Key));
        while (oldLocation < old.Length) {
            bool rep = false;
            foreach (var pair in nonNull) {
                string oldValue = pair.Key;
                string newValue = pair.Value;

                if (old.AtLocation(oldValue, oldLocation)) {
                    ret.Add(Tuple.Create(oldLocation, oldValue));
                    sb.Append(newValue);

                    oldLocation = oldLocation + oldValue.Length;

                    rep = true;
                    break;
                }
            }
            if (!rep) {
                sb.Append(old[oldLocation]);
                oldLocation++;
            }
        }

        newString = sb.ToString();
        return ret;
    }

    public static Rectangle ExpandToPoint(this Rectangle rect, int x, int y) {
        int newUpperLeftX = (int)Math.Min(x, rect.X);
        int newUpperLeftY = (int)Math.Min(y, rect.Y);
        int newLowerRightX = (int)Math.Max(x, rect.X + rect.Width);
        int newLowerRightY = (int)Math.Max(y, rect.Y + rect.Height);

        return new Rectangle(newUpperLeftX, newUpperLeftY, newLowerRightX - newUpperLeftX + 1, newLowerRightY - newUpperLeftY + 1);
    }

    /// <summary>
    /// Converts an IEnumberable into a 2D array.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the IEnumberable and returned array.</typeparam>
    /// <param name="list">The IEnumberable to convert.</param>
    /// <param name="width">The width of the returned array.</param>
    /// <param name="height">The height of the returned array.</param>
    /// <returns>A 2D array, [width,height], created from the IEnumerable. Goes left-to-right, top-to-bottom.</returns>
    public static T[,] ToArray<T>(this IEnumerable<T> list, int width, int height) {
        T[,] arr = new T[width, height];
        int x = 0;
        int y = 0;
        try {
            foreach (T value in list) {
                arr[x, y] = value;
                x++;
                if (x == width) {
                    x = 0;
                    y++;
                }
            }
        } catch {
            throw;
        }

        return arr;
    }

    public static List<List<T>> To2DList<T>(this IEnumerable<T> list, int width, int height) {
        List<List<T>> arr = new List<List<T>>();
        try {
            using (var enumerator = list.GetEnumerator()) {
                for (int i = 0; i < width * height; i++) {
                    if (i < width) {
                        arr.Add(new List<T>(height));
                    }
                    enumerator.MoveNext();
                    arr[i % width].Add(enumerator.Current);
                }
            }
        } catch {
            throw;
        }

        return arr;
    }

    /// <summary>
    /// Swaps two values.
    /// </summary>
    /// <typeparam name="T">The type of the items we're swapping.</typeparam>
    /// <param name="item1">The first item.</param>
    /// <param name="item2">The second item.</param>
    public static void Swap<T>(ref T item1, ref T item2) {
        T temp = item1;
        item1 = item2;
        item2 = temp;
    }

    public static void PosterizeImage(this Bitmap image, Rectangle rect, int tolerance = 127) {
        for (int i = rect.X; i < rect.Width; i++) {
            for (int j = rect.Y; j < rect.Height; j++) {
                Color c = image.GetPixel(i, j);
                if ((c.R + c.G + c.B) / 3 > tolerance) {
                    image.SetPixel(i, j, Color.White);
                } else {
                    image.SetPixel(i, j, Color.Black);
                }
            }
        }
    }

    public static void PosterizeImage(this Bitmap image, int tolerance = 127) {
        image.PosterizeImage(new Rectangle(0, 0, image.Width, image.Height), tolerance);
    }

    /// <summary>
    /// Squares a number.
    /// </summary>
    /// <param name="i">The number to square.</param>
    /// <returns>The number squared.</returns>
    public static int Square(this int i) {
        return i * i;
    }

    /// <summary>
    /// Raises a number to a power.
    /// </summary>
    /// <param name="i">The number.</param>
    /// <param name="power">The power to raise the number to.</param>
    /// <returns>The number raised to the power.</returns>
    /// <exception cref="System.OverflowException" />
    public static int Power(this int i, int power) {
        int ret = 1;
        for (int time = 0; time < power; time++) {
            ret *= i;
            if (ret < 0) {
                throw new OverflowException();
            }
        }

        return ret;
    }

    /// <summary>
    /// Calculates the distance between two colors.
    /// </summary>
    /// <param name="color1">The first color.</param>
    /// <param name="color2">The color to calculate the distance from.</param>
    /// <returns>The Euclidean distance between the two colors, based on RGB value.</returns>
    public static double Distance(this Color color1, Color color2) {
        return Math.Sqrt((color1.R - color2.R).Square() + (color1.G - color2.G).Square() + (color1.B - color2.B).Square());
    }

    public static double HSVDistance(this Color color1, Color color2) {
        return Math.Sqrt((color1.GetHue() - color2.GetHue()).Square() + 
            (color1.GetSaturation() - color2.GetSaturation()).Square() + 
            (color1.GetBrightness() - color2.GetBrightness()).Square()
        );
    }

    public static float Square(this float f) { return f * f; }

    public static Color ColorFrom565(int r, int g, int b) {
        return Color.FromArgb((r << 3) | (r >> 2), (g << 2) | (g >> 4), (b << 3) | (b >> 2));
    }


    public static Tuple<byte, byte> Color565FromRGB(int rgb) {
        int r = (rgb & 0xFF0000) >> 16;
        int g = (rgb & 0x00FF00) >> 8;
        int b = (rgb & 0x0000FF); 

        int newR = r >> 3;
        int newG = g >> 2;
        int newB = b >> 3;

        int newC = (newR << 11) | (newG << 5) | newB;

        return Tuple.Create((byte)(newC >> 8), (byte)(newC));
    }
    
    public static Color ColorFrom565(byte b1, byte b2) {
        int pixel = (b1 << 8) | (b2);
        int redMask = 0xF800;
        int greenMask = 0x7E0;
        int blueMask = 0x1F;
        return Color.FromArgb((pixel & redMask) >> 8, (pixel & greenMask) >> 3, (pixel & blueMask) << 3);
    }

    public static Color ColorFrom8BitHLRGB(int value) {
        value = value | (value << 8);
        return ColorFrom565(value >> 11, (value & 0x7E0) >> 5, value & 0x1F);
    }

    public static Color ColorFrom8BitHLBGR(int value) {
        value = value | (value << 8);
        return ColorFrom565(value & 0x1F, (value & 0x7E0) >> 5, value >> 11);
    }

    public static void PosterizeImage(this Bitmap image, Rectangle rect, List<Color> colors) {
        for (int i = 0; i < rect.Width; i++) {
            for (int j = 0; j < rect.Height; j++) {
                Color pixelColor = image.GetPixel(i, j);
                Color minColor = Color.White;
                double minDistance = double.MaxValue;
                foreach (Color color in colors) {
                    double distance = pixelColor.Distance(color);
                    if (distance < minDistance) {
                        minColor = color;
                        minDistance = distance;
                    }
                }

                image.SetPixel(i, j, minColor);
            }
        }
    }

    public static int[,] PalettizeImage(this Bitmap image, List<Color> colors, int transparent = -1) {
        int[,] sprite = new int[image.Width, image.Height];
        Dictionary<Color, int> colorCache = new Dictionary<Color, int>();
        for (int i = 0; i < image.Width; i++) {
            for (int j = 0; j < image.Height; j++) {
                Color pixelColor = image.GetPixel(i, j);
                int minColor = -1;

                if (transparent != -1 && pixelColor.A == 0) {
                    sprite[i, j] = transparent;
                    continue;
                }

                if (colorCache.ContainsKey(pixelColor)) {
                    sprite[i, j] = colorCache[pixelColor];
                    continue;
                }

                double minDistance = double.MaxValue;
                for (int colorIndex = 0; colorIndex < colors.Count; colorIndex++) {
                    //if (colorIndex == transparent) { continue; }
                    Color color = colors[colorIndex];
                    double distance = pixelColor.Distance(color);
                    if (distance <= minDistance) {
                        minColor = colorIndex;
                        minDistance = distance;
                    }
                }

                colorCache[pixelColor] = minColor;
                sprite[i, j] = minColor;
            }
        }

        return sprite;
    }

    public static string GetFileName(this FileInfo info) {
        return info.Name.Remove(info.Name.Length - info.Extension.Length);
    }

    public static string ClippedSubstring(this string s, int startIndex, int length) {
        if (startIndex > s.Length) { return null; }

        if (startIndex + length > s.Length) {
            return s.Substring(startIndex);
        } else {
            return s.Substring(startIndex, length);
        }
    }

    public static string GetAttributeOrDefault(this XmlNode node, string attribute, string def) {
        if (node.Attributes[attribute] == null) { return def; }
        return node.Attributes[attribute].Value;
    }

    public static int GetAttributeOrDefault(this XmlNode node, string attribute, int def) {
        int ret;
        string val = GetAttributeOrDefault(node, attribute, def.ToString());
        return int.TryParse(val, out ret) ? ret : def;
    }

    public static bool GetAttributeOrDefault(this XmlNode node, string attribute, bool def) {
        bool ret;
        string val = GetAttributeOrDefault(node, attribute, def.ToString());
        return bool.TryParse(val, out ret) ? ret : def;
    }

    /// <summary>
    /// Adds a filter to the FileDialog.
    /// </summary>
    /// <param name="text">The text to display.</param>
    /// <param name="ext">The extensions.</param>
    public static void AddFilter(this FileDialog ofd, string text, params string[] ext) {
        StringBuilder compiledExt = new StringBuilder(6 * ext.Length);
        for (int i = 0; i < ext.Length; i++) {
            compiledExt.AppendFormat("{1}{0}", ext[i], i == 0 ? "" : ";");
        }
        ofd.Filter = string.Concat(ofd.Filter, ofd.Filter != "" ? "|" : "", text, "|", compiledExt.ToString());
    }

    /// <summary>
    /// Read an array of elements from a <see cref="BinaryReader"/>.
    /// </summary>
    /// <typeparam name="T">The type of element to read.</typeparam>
    /// <param name="reader">The <see cref="BinaryReader"/> to read from.</param>
    /// <param name="count">The number of elements to read.</param>
    /// <returns>An array of elements of type <typeparamref name="T"/>.</returns>
    public static T[] ReadArray<T>(this BinaryReader reader, int count) {
        var RawData = reader.ReadBytes(count * Marshal.SizeOf(typeof(T)));
        var Result = new T[count];
        var PinnedResult = GCHandle.Alloc(Result, GCHandleType.Pinned);
        try {
            Marshal.Copy(RawData, 0, PinnedResult.AddrOfPinnedObject(), RawData.Length);
        } finally {
            PinnedResult.Free();
        }
        return Result;
    }

    public static byte[] ToByteArray(this int i, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.GetBytes(i);
        } else {
            byte[] b = BitConverter.GetBytes(i);
            Array.Reverse(b);
            return b;
        }
    }

    public static byte[] ToByteArray(this short s, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.GetBytes(s);
        } else {
            byte[] b = BitConverter.GetBytes(s);
            Array.Reverse(b);
            return b;
        }
    }

    public static byte[] ToByteArray(this ushort s, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.GetBytes(s);
        } else {
            byte[] b = BitConverter.GetBytes(s);
            Array.Reverse(b);
            return b;
        }
    }

    public static byte[] ToByteArray(this uint s, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.GetBytes(s);
        } else {
            byte[] b = BitConverter.GetBytes(s);
            Array.Reverse(b);
            return b;
        }
    }

    public static ushort ToUShort(this byte[] b, int index, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.ToUInt16(b, index);
        } else {
            byte[] sub = b.SubArray(index, 2);
            Array.Reverse(sub);
            return BitConverter.ToUInt16(sub, 0);
        }
    }

    public static uint ToUInt(this byte[] b, int index, bool bigEndian = false) {
        if (!bigEndian) {
            return BitConverter.ToUInt32(b, index);
        } else {
            byte[] sub = b.SubArray(index, 4);
            Array.Reverse(sub);
            return BitConverter.ToUInt32(sub, 0);
        }
    }

    /// <summary>
    /// Gets a subset of the given length starting at the given index.
    /// </summary>
    /// <typeparam name="T">The type of the array.</typeparam>
    /// <param name="data">The array you want to get the subset from.</param>
    /// <param name="index">The start index to copy from.</param>
    /// <param name="length">The length of the array to return.</param>
    /// <returns>A subset from the original array.</returns>
    public static T[] SubArray<T>(this T[] data, int index, int length = -1) {
        if (length == -1)
            length = data.Length - index;
        T[] result = new T[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }

    /// <summary>
    /// Calculates the checksum of a byte array.
    /// </summary>
    /// <param name="buffer">The array of bytes on which to calculate the checksum.</param>
    /// <returns>The last two bytes (as a short) of the sum of all the bytes in the buffer.</returns>
    public static short Checksum(this IEnumerable<byte> buffer) {
        Int64 sum = 0;
        foreach (byte b in buffer) {
            sum += b;
        }
        return (short)(sum % 65536);
    }

    /// <summary>
    /// Gets the bytes that make up the givens short.
    /// </summary>
    /// <param name="s">The short to get the bytes from.</param>
    /// <returns>The two-byte array that this short is made up of.</returns>
    public static byte[] GetBytes(this short s) {
        return BitConverter.GetBytes(s);
    }

    /// <summary>
    /// Converts this string into an array of ASCII bytes.
    /// </summary>
    /// <param name="s">The string to convert</param>
    /// <param name="arrayLen">How long the returned array should be. If it's longer than the string, it will pad the string right with the pad char.</param>
    /// <param name="padChar">The character used for padding the string.</param>
    /// <returns>An array of bytes representing the string in ASCII.</returns>
    public static byte[] ToByteArray(this string s, int arrayLen = -1, char padChar = '\0') {
        if (arrayLen == -1)
            arrayLen = s.Length;
        byte[] arr = new byte[arrayLen];
        ASCIIEncoding.ASCII.GetBytes(s.PadRight(arrayLen, padChar), 0, arrayLen, arr, 0);
        return arr;
    }

    public static byte[] ToPackedByteArray(this string s) {
        int NumberChars = s.Length;
        byte[] bytes = new byte[NumberChars / 2];
        for (int i = 0; i < NumberChars; i += 2)
            bytes[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
        return bytes;
    }

    public static string AsString(this byte[] array) {
        StringBuilder sb = new StringBuilder();
        foreach (byte b in array) {
            sb.Append(b.ToString("X2"));
        }

        return sb.ToString();
    }

    public static string ToAsciiString(this byte[] b) {
        return ASCIIEncoding.ASCII.GetString(b);
    }

    public static string ToUtf8String(this byte[] b) {
        return UTF8Encoding.UTF8.GetString(b);
    }

    public static void FloodFill(this Bitmap b, Pen pen, int x, int y) {
        if (b.GetPixel(x, y).ToArgb().Equals(pen.Color.ToArgb()))
            return;
        Stack<Point> s = new Stack<Point>();
        s.Push(new Point(x, y));
        while (s.Count > 0) {
            Point p = s.Pop();
            if (p.X < 0 || p.Y < 0 || p.X >= b.Width || p.Y >= b.Height)
                continue;
            if (b.GetPixel(p.X, p.Y).ToArgb().Equals(pen.Color.ToArgb()))
                continue;
            b.SetPixel(p.X, p.Y, pen.Color);
            s.Push(new Point(p.X + 1, p.Y));
            s.Push(new Point(p.X - 1, p.Y));
            s.Push(new Point(p.X, p.Y + 1));
            s.Push(new Point(p.X, p.Y - 1));
        }
    }

    /// <summary>
    /// Draws an empty rectangle to the screen.
    /// </summary>
    /// <param name="g">The graphics object to draw onto.</param>
    /// <param name="p">The pen that defines the color, width, and style of the rectange.</param>
    /// <param name="x1">The X value of a corner of the rectangle.</param>
    /// <param name="y1">The Y value of a corner of the rectangle.</param>
    /// <param name="x2">The X value of the opposite corner of the rectangle.</param>
    /// <param name="y2">The Y value of the opposite corner of the rectangle.</param>
    public static void DrawRect(this Graphics g, Pen p, int x1, int y1, int x2, int y2) {
        using (GraphicsPath gp = new GraphicsPath()) {
            gp.StartFigure();
            gp.AddLine(x1, y1, x2, y1);
            gp.AddLine(x2, y2, x2, y2);
            gp.AddLine(x2, y2, x1, y2);
            gp.CloseFigure();
            g.DrawPath(p, gp);
        }
    }

    /// <summary>
    /// Draws a filled rectangle to the screen
    /// </summary>
    /// <param name="g">The graphics object to draw onto.</param>
    /// <param name="b">The brush that determines the characteristics of the rectangle.</param>
    /// <param name="x1">The X value of a corner of the rectangle.</param>
    /// <param name="y1">The Y value of a corner of the rectangle.</param>
    /// <param name="x2">The X value of the opposite corner of the rectangle.</param>
    /// <param name="y2">The Y value of the opposite corner of the rectangle.</param>
    public static void FillRect(this Graphics g, Brush b, int x1, int y1, int x2, int y2) {
        using (GraphicsPath gp = new GraphicsPath()) {
            gp.StartFigure();
            gp.AddLine(x1, y1, x2, y1);
            gp.AddLine(x2, y2, x2, y2);
            gp.AddLine(x2, y2, x1, y2);
            gp.CloseFigure();
            g.FillPath(b, gp);
        }
    }

    public static void DrawCircle(this Graphics g, Pen p, int x, int y, int r) {
        g.DrawEllipse(p, x - r, y - r, r * 2, r * 2);
    }

    public static void FillCircle(this Graphics g, Brush b, int x, int y, int r) {
        g.FillEllipse(b, x - r, y - r, r * 2, r * 2);
    }
}
//}