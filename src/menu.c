#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <debug.h>

#include <lib/ce/graphc.h>
#include <lib/ce/fileioc.h>

#include "key_helper.h"
#include "menu.h"
#include "menu_sprites.h"

char* menu_back_string = "Back";

Menu* menu_create(uint8_t numItems, const char* title) {
    uint8_t i;

    Menu* menu = malloc(numItems * sizeof(Menu));
    menu->Items = malloc(numItems * sizeof(MenuItem));
    for (i = 0; i < numItems; i++) {
        menu->Items[i].Function = MENU_FUNCTION_NONE;
        menu->Items[i].Name = "";
        menu->Items[i].Selected = false;
        menu->Items[i].Tag = NULL;
		menu->Items[i].Icon = NULL;
		menu->Items[i].IconWidth = 0;
		menu->Items[i].IconHeight = 0;
    }
    menu->NumItems = numItems;

    menu->Title = title;
    menu->ExtraFunction = MENU_FUNCTION_NONE;
    menu->BackKey = 0;
    menu->CursorChar = 0x10;
    menu->Tag = NULL;

    menu->XLocation = 0;
    menu->YLocation = 0;
    menu->ClearScreen = true;
    menu->ClearColor = 255;
    menu->TextBackgroundColor = 255;
    menu->TextForegroundColor = 0;

	menu->LineSpacing = 10;
	menu->TextSpacing = 0;

	menu->Icon = NULL;
	menu->IconWidth = 0;
	menu->IconHeight = 0;
	menu->Cursor = NULL;
	menu->CursorWidth = 0;
	menu->CursorHeight = 0;
	menu->CursorXOff = 0;
	menu->CursorYOff = 0;

	menu->RepeatKey = false;
	menu->KeyDelay = 0;
	menu->NumExtraKeys = 0;
	menu->ExtraKeyVector = NULL;

    return menu;
}

void menu_allocate_key_vector(Menu* menu, uint8_t numKeys) {
	if (menu == NULL) { return; }
	if (menu->ExtraKeyVector != NULL) { free(menu->ExtraKeyVector); }
	menu->ExtraKeyVector = malloc(numKeys * sizeof(MenuKey));
	menu->NumExtraKeys = numKeys;

	for (numKeys = 0; numKeys < menu->NumExtraKeys; numKeys++) {
		menu->ExtraKeyVector[numKeys].Function = MENU_FUNCTION_NONE;
		menu->ExtraKeyVector[numKeys].Key = 0;
		menu->ExtraKeyVector[numKeys].RepeatKey = menu->RepeatKey;
	}
}

void menu_delete(Menu* menu) {
	if (menu->ExtraKeyVector != NULL) { free(menu->ExtraKeyVector); }
    free(menu->Items);
    free(menu);
}

void handle_function(void* function, Menu* menu, uint32_t* frameNumber, uint8_t* y, bool* back) {
	MenuEventArgs* eventArgs;
	void(*func)(MenuEventArgs*) = function;
	
	*back = false;
	if (func == MENU_FUNCTION_NONE) {
		return;
	} else if (func == MENU_FUNCTION_BACK) {
		*back = true;
		return;
	}

	eventArgs = malloc(sizeof(MenuEventArgs));

	if (eventArgs == NULL) {
		dbg_printf(dbgout, "Failed to allocate event args.\n");
		debugger();
	}

	func = function;
	eventArgs->FrameNumber = *frameNumber;
	eventArgs->Menu = menu;
	eventArgs->Index = *y - 1;
	eventArgs->Back = false;
	eventArgs->Redraw = false;

	func(eventArgs);
	*y = eventArgs->Index + 1;
	*frameNumber = eventArgs->FrameNumber;
	*back = eventArgs->Back;

	if (eventArgs->Redraw) {
		gc_FillScrn(menu->ClearColor);
	}

	free(eventArgs);
}

int menu_display(Menu* menu) {
    uint8_t i;
    uint8_t y = 1;
    uint8_t old_y = 1;
    uint8_t linePadding;
    uint8_t textPadding;
	MenuItem* item;
    uint8_t extraTextPadding = 0;
    uint8_t previouslySelectedIndex = 0;
    bool selected = false;
    uint32_t frameNumber = 0;
    bool back = false;
	bool(*keyFunc)(key_t);

	linePadding = menu->LineSpacing;
	textPadding = menu->TextSpacing + 10;

    if (menu->SelectionType != MenuSelectionType_None) {
        extraTextPadding = linePadding;
    }

    if (menu->ClearScreen) {
        gc_FillScrn(menu->ClearColor);
    }

    while (!back) {
		keyFunc = menu->RepeatKey ? key_is_down : key_just_pressed;
        gc_SetTextColor(menu->TextForegroundColor | menu->TextBackgroundColor << 8);

        if (menu->Title != NULL) {
            gc_PrintStringXY(menu->Title, menu->XLocation + 2, menu->YLocation + 1);
            gc_SetColorIndex(menu->TextForegroundColor);
            gc_NoClipHorizLine(menu->XLocation + 1, menu->YLocation + 10, gc_StringWidth(menu->Title) + 5);
        }

		if (menu->Icon != NULL) {
			gc_NoClipDrawSprite(menu->Icon, menu->XLocation + 2, menu->YLocation + 1, menu->IconWidth, menu->IconHeight);
		}

        for (i = 0; i < menu->NumItems; i++) {
			item = &(menu->Items[i]);
			if (item->Name != NULL) {
				gc_PrintStringXY(item->Name, menu->XLocation + textPadding + extraTextPadding, menu->YLocation + 3 + linePadding + linePadding * i);
			}

			if (item->Icon != NULL) {
				gc_NoClipDrawSprite(item->Icon, menu->XLocation + 10, menu->YLocation + 3 + linePadding + linePadding * i, item->IconWidth, item->IconHeight);
			}

            if (menu->SelectionType != MenuSelectionType_None && item->Function != MENU_FUNCTION_BACK) {
                if (item->Selected) {
                    previouslySelectedIndex = i;
                    selected = true;
                } else {
                    selected = false;
                }

                switch (menu->SelectionType) {
                    case MenuSelectionType_Single:
                        gc_NoClipDrawSprite(selected ? radiobutton_filled : radiobutton_empty, menu->XLocation + textPadding, menu->YLocation + 3 + linePadding + linePadding * i - 1, 9, 9);
                        break;
                    case MenuSelectionType_Multiple:
                        gc_NoClipDrawSprite(selected ? checkbox_checked : checkbox_empty, menu->XLocation + textPadding, menu->YLocation + 3 + linePadding + linePadding * i - 1, 9, 9);
                        break;
                }
            }
        }

		if (menu->Cursor == NULL) {
			gc_SetTextXY(menu->XLocation + 2, menu->YLocation + 3 + linePadding * y);
			gc_PrintChar(menu->CursorChar);
		} else {
			gc_NoClipDrawSprite(menu->Cursor, menu->XLocation + 2 + menu->CursorXOff, menu->YLocation + 3 + linePadding * y + menu->CursorYOff, menu->CursorWidth, menu->CursorHeight);
		}

        key_scan_keys(menu->KeyDelay);
        old_y = y;
        
		handle_function(menu->ExtraFunction, menu, &frameNumber, &y, &back);

        if (keyFunc(Key_Up)) {
			if (y == 1) {
				handle_function(menu->PreviousPageFunction, menu, &frameNumber, &y, &back);
				y = menu->NumItems;
			} else {
				y = y - 1;
			}
		}
		else if (keyFunc(Key_Down)) {
			if (y == menu->NumItems) {
				handle_function(menu->NextPageFunction, menu, &frameNumber, &y, &back);
				y = 1;
			} else {
				y = y + 1;
			}
		} else if (keyFunc(Key_Right)) {
			handle_function(menu->NextPageFunction, menu, &frameNumber, &y, &back);
			y = 1;
		} else if (keyFunc(Key_Left)) {
			handle_function(menu->PreviousPageFunction, menu, &frameNumber, &y, &back);
			y = 1;
		} else if (keyFunc(Key_2nd) || keyFunc(Key_Enter)) {
            uint8_t index = y - 1;

            if (menu->SelectionType != MenuSelectionType_None && menu->Items[y - 1].Function != MENU_FUNCTION_BACK) {
                switch (menu->SelectionType) {
                    case MenuSelectionType_Single:
                        if (index != previouslySelectedIndex) {
                            menu->Items[previouslySelectedIndex].Selected = false;
                            menu->Items[index].Selected = true;
                        }
                        break;
                    case MenuSelectionType_Multiple:
                        menu->Items[index].Selected = !menu->Items[index].Selected;
                        break;
                }
            } 
            
			handle_function(menu->Items[index].Function, menu, &frameNumber, &y, &back);
            gc_FillScrn(menu->ClearColor);
        } else if (keyFunc(menu->BackKey)) {
            y = 0;
            back = true;
        }

		for (i = 0; i < menu->NumExtraKeys; i++) {
			bool keyDown = false;
			MenuKey* k = &(menu->ExtraKeyVector[i]);
			if (k->RepeatKey) {
				keyDown = key_is_down(k->Key);
			} else {
				keyDown = key_just_pressed(k->Key);
			}

			if (keyDown) {
				handle_function(k->Function, menu, &frameNumber, &y, &back);
			}
		}

        if (old_y != y) {
            gc_SetColorIndex(menu->ClearColor);
			if (menu->Cursor == NULL) {
				gc_NoClipRectangle(menu->XLocation + 0, menu->YLocation + 3 + linePadding * old_y, 10, 8);
			} else {
				gc_NoClipRectangle(menu->XLocation + 2 + menu->CursorXOff, menu->YLocation + 3 + linePadding * old_y + menu->CursorYOff, menu->CursorWidth, menu->CursorHeight);
			}
        }

        frameNumber++;
    }
    return y-1;
}