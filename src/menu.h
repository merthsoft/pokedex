#ifndef MENU_H
#define MENU_H

#include <stdint.h>
#include <stdlib.h>

#include "key_helper.h"

#define MENU_FUNCTION_BACK (void*)1
#define MENU_FUNCTION_NONE (void*)0

typedef struct MenuItem {
    char* Name;
    void* Function;
    bool Selected;
    void* Tag;
	uint8_t* Icon;
	uint8_t IconWidth;
	uint8_t IconHeight;
} MenuItem;

typedef struct MenuKey {
	key_t Key;
	void* Function;
	bool RepeatKey;
} MenuKey;

typedef enum MenuSelectionType {
    MenuSelectionType_None,
    MenuSelectionType_Single,
    MenuSelectionType_Multiple
} MenuSelectionType;

typedef struct Menu {
	bool RepeatKey;
	uint8_t KeyDelay;
	uint8_t NumExtraKeys;
	MenuKey* ExtraKeyVector;

    uint16_t XLocation;
    uint8_t YLocation;
        
    uint8_t ClearColor;
    bool ClearScreen;

	uint8_t LineSpacing;
	uint8_t TextSpacing;

    uint8_t TextForegroundColor;
    uint8_t TextBackgroundColor;
    char* Title;

	uint8_t* Icon;
	uint8_t IconWidth;
	uint8_t IconHeight;

	uint8_t* Cursor;
	uint8_t CursorWidth;
	uint8_t CursorHeight;
	int8_t CursorXOff;
	int8_t CursorYOff;

    uint8_t NumItems;
    MenuItem* Items;
    
    void* ExtraFunction;
    
    MenuSelectionType SelectionType;
    
    key_t BackKey;
    char CursorChar;
    
    void* Tag;

	void* NextPageFunction;
	void* PreviousPageFunction;
} Menu;

typedef struct MenuEventArgs {
    Menu* Menu;
    uint8_t Index;
    uint32_t FrameNumber;
    bool Back;
	bool Redraw;
} MenuEventArgs;

Menu* menu_create(uint8_t numItems, const char* title);
void menu_allocate_key_vector(Menu* menu, uint8_t numKeys);
void menu_delete(Menu* menu);
int menu_display(Menu* menu);

#endif MENU_H