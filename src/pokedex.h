#ifndef _POKEDEX_H
#define _POKEDEX_H

/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Shared libraries */
#include <lib\ce\fileioc.h>

typedef struct PokedexEntry {
    uint16_t id;
    uint8_t isDefault;

    char* name;
    char* genus;
	char* listDisplayName;

	uint8_t statsAppvarIndex;
    uint16_t statsLocation;

	uint8_t iconWidth;
	uint8_t iconHeight;
    uint8_t* iconData;

	char* description;
	uint8_t type1;
	uint16_t type2;

	uint16_t statsOffset;

	struct PokedexEntry* nonDefaultPokemon;
} PokedexEntry;

typedef struct PokemonStatsData {
	uint16_t height;
	uint16_t weight;
	uint8_t hp;
	uint8_t attack;
	uint8_t defense;
	uint8_t specialAttack;
	uint8_t specialDefense;
	uint8_t speed;

	uint16_t ability1;
	uint16_t ability2;
	uint16_t ability3;
} PokemonStatsData;

typedef struct PokemonType {
	uint8_t id;
	char* name;

	uint8_t iconWidth;
	uint8_t iconHeight;
	uint8_t* iconData;
} PokemonType;

typedef struct Pokedex {
	uint16_t numPokemon;
	PokedexEntry* pokemon;
	
	uint8_t logoWidth;
	uint8_t logoHeight;
	uint8_t* logo;

	uint8_t cursorWidth;
	uint8_t cursorHeight;
	uint8_t* cursor;

	uint8_t numTypes;
	PokemonType* types;

	ti_var_t dataFile;
	uint8_t currentIndex;
	ti_var_t mainFile;
	uint16_t abilityDataLocation;
} Pokedex;

Pokedex* pokedex_start(void* logo_loaded_call_back);
void pokedex_close(Pokedex* pokedex);

void pokedex_read_initial_data(Pokedex* pokedex, PokedexEntry* entry);
PokemonStatsData* pokedex_read_stats(Pokedex* pokedex, PokedexEntry* entry);
char* pokedex_read_ability_name(Pokedex* pokedex, uint16_t id);

void pokedex_delete_initial_data(PokedexEntry* entry);
void pokedex_delete_entry(PokedexEntry* entry);

#endif