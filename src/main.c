/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Shared libraries */
#include <lib\ce\graphc.h>

#include "main.h"
#include "pokedex.h"
#include "key_helper.h"
#include "menu.h"

void main(void) {
	Menu* mainMenu;
	Pokedex* pokedex;
    
    Key_init();
    gc_InitGraph();

    gc_FillScrn(BACKGROUND_COLOR);
    
	pokedex = pokedex_start(draw_logo_while_loading);
	mainMenu = init_menu(pokedex);

	menu_display(mainMenu);
	
	menu_delete(mainMenu);
    pokedex_close(pokedex);
	
    key_reset();
    gc_CloseGraph();
    pgrm_CleanUp();
}

Menu* init_menu(Pokedex* pokedex) {
	Menu* mainMenu;
	MenuTag* menuTag = malloc(sizeof(MenuTag));

	menuTag->pokedex = pokedex;
	menuTag->topPokemon = 0;
	menuTag->selectedPokemon = 0xFFFF;

	mainMenu = menu_create(DISPLAY_SIZE, NULL);
	mainMenu->Icon = pokedex->logo;
	mainMenu->IconWidth = pokedex->logoWidth;
	mainMenu->IconHeight = pokedex->logoHeight;

	mainMenu->Cursor = pokedex->cursor;
	mainMenu->CursorWidth = pokedex->cursorWidth;
	mainMenu->CursorHeight = pokedex->cursorHeight;
	mainMenu->CursorXOff = -10;
	mainMenu->CursorYOff = 1;

	mainMenu->ExtraFunction = draw_large_icon;
	mainMenu->XLocation = 145;
	mainMenu->YLocation = 0;
	mainMenu->BackKey = Key_Clear;
	mainMenu->LineSpacing = 31;
	mainMenu->TextSpacing = 50;

	load_menu_items(mainMenu, pokedex, 0);

	mainMenu->Tag = menuTag;
	mainMenu->NextPageFunction = next_page;
	mainMenu->PreviousPageFunction = prev_page;

	mainMenu->RepeatKey = true;

	menu_allocate_key_vector(mainMenu, 5);
	mainMenu->ExtraKeyVector[0].Key = Key_Graph;
	mainMenu->ExtraKeyVector[0].Function = next_page;

	mainMenu->ExtraKeyVector[1].Key = Key_Trace;
	mainMenu->ExtraKeyVector[1].Function = prev_page;

	mainMenu->ExtraKeyVector[2].Key = Key_Yequ;
	mainMenu->ExtraKeyVector[2].Function = select_pokemon;
	mainMenu->ExtraKeyVector[2].RepeatKey = false;

	mainMenu->RepeatKey = false;

	return mainMenu;
}

void print_stat(uint16_t x, uint8_t* y, char* statName, char* statFormat, ...) {
	char* text = malloc(256);
	va_list myargs;
	va_start(myargs, statFormat);
	vsprintf(text, statFormat, myargs);
	
	gc_PrintStringXY(statName, x, *y);
	gc_PrintStringXY(text, x + 125, *y);
	*y += 10;
	
	free(text);
	va_end(myargs);
}

void draw_section_header(uint16_t x, uint8_t* y, char* header) {
	uint16_t width = gc_StringWidth(header);
	gc_NoClipRectangle(x, *y, width + 8, 14);

	gc_SetTextColor(254 | FOREGROUND_COLOR << 8);
	gc_PrintStringXY(header, x + 3, *y + 3);
	gc_SetTextColor(FOREGROUND_COLOR | BACKGROUND_COLOR << 8);
	*y += 16;
}

void select_pokemon(MenuEventArgs* menuArgs) {
	Menu* mainMenu;
	MenuTag* tag;
	PokedexEntry* entry;
	PokemonStatsData* data;
	Pokedex* pokedex;
	uint16_t x;
	uint8_t y;
	uint16_t* ability;
	uint8_t i;
	char* abilityName;
	char* options[] = { "Back", "Evos.", "", "Prev", "Next" };

	mainMenu = menuArgs->Menu;
	tag = mainMenu->Tag;
	pokedex = tag->pokedex;
	entry = mainMenu->Items[menuArgs->Index].Tag;

	gc_FillScrn(BACKGROUND_COLOR);
	tag->selectedPokemon = 0xFFFF;
	draw_large_icon(menuArgs);
	draw_function_keys(options);

	gc_NoClipDrawSprite(mainMenu->Icon, mainMenu->XLocation + 2, mainMenu->YLocation + 1, mainMenu->IconWidth, mainMenu->IconHeight);
	data = pokedex_read_stats(tag->pokedex, entry);

	x = mainMenu->XLocation + 2;
	y = mainMenu->YLocation + 1 + mainMenu->IconHeight;

	draw_section_header(x, &y, "Stats");
		
	print_stat(x, &y, "Height", "%i", data->height);
	print_stat(x, &y, "Weight", "%i", data->weight);
	
	print_stat(x, &y, "HP", "%i", data->hp);
	print_stat(x, &y, "Attack", "%i", data->attack);
	print_stat(x, &y, "Defense", "%i", data->defense);
	print_stat(x, &y, "Special Attack", "%i", data->specialAttack);
	print_stat(x, &y, "Special Defense", "%i", data->specialDefense);
	print_stat(x, &y, "Speed", "%i", data->speed);

	gc_NoClipRectangle(x - 2, y, 147, 2);
	y += 4;

	draw_section_header(x, &y, "Abilities");

	ability = &data->ability1;
	for (i = 0; i < 3; i++) {
		if (ability[i] != 0) {
			abilityName = pokedex_read_ability_name(pokedex, ability[i]);
			if (abilityName != NULL) {
				print_stat(x, &y, abilityName, "");
				free(abilityName);
			}
		}
	}

	free(data);
	
	do {
		key_scan_keys(0);
	} while (!key_just_pressed(Key_Yequ) && !key_just_pressed(Key_Clear));
	tag->selectedPokemon = 0xFFFF;
	gc_FillScrn(BACKGROUND_COLOR);
}

void draw_logo_while_loading(uint8_t* logo, uint8_t width, uint8_t height) {
	uint16_t x, y, scaledWidth, scaledHeight;
	char* text;
	
	scaledWidth = width * 3;
	scaledHeight = height * 3;
	x = center(LCD_WIDTH_PX, scaledWidth);
	y = center(LCD_HEIGHT_PX, scaledHeight);

	gc_NoClipDrawScaledSprite(logo, x, y, width, height, 3, 3);
	
	gc_PrintStringXY("Merthsoft  '16", 1, LCD_HEIGHT_PX - 8);
	text = "v0.1";
	gc_PrintStringXY(text, LCD_WIDTH_PX - gc_StringWidth(text) - 2, LCD_HEIGHT_PX - 8);
}

void load_menu_items(Menu* mainMenu, Pokedex* pokedex, uint16_t top) {
	uint8_t i;
	PokedexEntry* entry;

	for (i = 0; i < DISPLAY_SIZE; i++) {
		pokedex_delete_initial_data(mainMenu->Items[i].Tag);
	}

	for (i = 0; i < DISPLAY_SIZE; i++) {
		entry = &(pokedex->pokemon[top + i]);
		pokedex_read_initial_data(pokedex, entry);
		mainMenu->Items[i].Name = entry->listDisplayName;
		mainMenu->Items[i].Tag = entry;
		mainMenu->Items[i].Icon = entry->iconData;
		mainMenu->Items[i].IconWidth = entry->iconWidth;
		mainMenu->Items[i].IconHeight = entry->iconHeight;
		mainMenu->Items[i].Function = select_pokemon;
	}
}

void next_page(MenuEventArgs* menuArgs) {
	Menu* mainMenu;
	MenuTag* tag;

	mainMenu = menuArgs->Menu;
	tag = mainMenu->Tag;

	if (tag->topPokemon + DISPLAY_SIZE == tag->pokedex->numPokemon) {
		tag->topPokemon = 0;
	} else if (tag->topPokemon + 2* DISPLAY_SIZE > tag->pokedex->numPokemon) {
		tag->topPokemon = tag->pokedex->numPokemon - DISPLAY_SIZE;
	} else {
		tag->topPokemon += DISPLAY_SIZE;
	}
	tag->selectedPokemon = 0xFFFF;

	load_menu_items(mainMenu, tag->pokedex, tag->topPokemon);

	menuArgs->Redraw = true;
}

void prev_page(MenuEventArgs* menuArgs) {
	Menu* mainMenu;
	MenuTag* tag;

	mainMenu = menuArgs->Menu;
	tag = mainMenu->Tag;

	if (tag->topPokemon == 0) {
		tag->topPokemon = tag->pokedex->numPokemon - DISPLAY_SIZE;
	} else if (tag->topPokemon < DISPLAY_SIZE) {
		tag->topPokemon = 0;
	} else {
		tag->topPokemon -= DISPLAY_SIZE;
	}
	tag->selectedPokemon = 0xFFFF;

	load_menu_items(mainMenu, tag->pokedex, tag->topPokemon);
	menuArgs->Redraw = true;
}

void new_line(int* currentLineLength, int indent, int baseX, int* x, int* y) {
	*currentLineLength = 8*indent;
	*x = baseX + 8 * indent;
	*y += 8;
}

void word_wrap(char* s, int lineLength, int indent, int tabLength, int x, int y) {
	int currentIndex = 0;
	int previousLocation = 0;
	int currentLineLength = 0;
	int currentWordLength = 0;
	int numChars = 0;
	bool running = true;
	int i;
	int baseX = x;

	while (running) {
		char c = s[currentIndex];
		if (c == ' ' || c == '\0' || c == '\n' || c == '\t') {
			if (currentLineLength + currentWordLength >= lineLength) {
				new_line(&currentLineLength, indent, baseX, &x, &y);
			}

			for (i = 0; i < numChars; i++) {
				gc_SetTextXY(x, y);
				gc_PrintChar(s[previousLocation + i]);
				x += gc_CharWidth(s[previousLocation + i]) + 1;
			}
			currentLineLength += currentWordLength;

			if (c == ' ') {
				currentLineLength += 8;
				x += 8;
			} else if (c == '\t') {
				/*int numSpaces = currentLineLength % tabLength;
				if (numSpaces == 0) { numSpaces = tabLength; }
				for (i = 0; i < numSpaces; i++) {
					currentLineLength += 8;
				}*/
			}

			previousLocation = currentIndex + 1;
			currentWordLength = 0;
			numChars = 0;

			if (c == '\n') {
				new_line(&currentLineLength, indent, baseX, &x, &y);
			}
		} else {
			currentWordLength += gc_CharWidth(c)+1;
			numChars++;
		}
		currentIndex++;

		if (c == '\0') { running = false; }
	}
}

void draw_function_keys(char* options[]) {
	char* text;
	uint8_t i;
	uint16_t x;

	gc_SetColorIndex(BACKGROUND_COLOR);
	gc_NoClipRectangle(0, 229, LCD_WIDTH_PX, 11);
	
	gc_SetColorIndex(FOREGROUND_COLOR);
	gc_NoClipHorizLine(0, 229, LCD_WIDTH_PX);
	gc_NoClipHorizLine(0, 230, LCD_WIDTH_PX);

	gc_NoClipVertLine(LCD_WIDTH_PX - 2, 231, 10);
	gc_NoClipVertLine(LCD_WIDTH_PX - 1, 231, 10);

	for (i = 0; i <= 5; i++) {
		x = i * 64;
		gc_NoClipVertLine(x, 231, 10);
		gc_NoClipVertLine(x + 1, 231, 10);

		if (i < 5) {
			text = options[i];
			x += center(62, gc_StringWidth(text));
			gc_PrintStringXY(text, x, 232);
		}
	}
}

void draw_large_icon(MenuEventArgs* menuArgs) {
	uint8_t width;
	uint8_t height;
	uint16_t x;
	uint8_t y;
	PokemonType* type;
	PokedexEntry* entry = menuArgs->Menu->Items[menuArgs->Index].Tag;
	MenuTag* menuTag = menuArgs->Menu->Tag;
	char* options[] = { "Select", "Goto...", "Search...", "Page Up", "Pg Down" };

	if (entry == NULL) { return; }
	if (entry->id == menuTag->selectedPokemon) { return; }
	menuTag->selectedPokemon = entry->id;

	width = entry->iconWidth;
	height = entry->iconHeight;

	x = center(LARGE_ICON_WIDTH, width * 3);
	y = center(LARGE_ICON_HEIGHT, height * 3);

	gc_SetColorIndex(BACKGROUND_COLOR);
	gc_NoClipRectangle(0, 0, LARGE_ICON_WIDTH + 20, 229);

	gc_NoClipDrawScaledSprite(entry->iconData, x, y, width, height, 3, 3);

	gc_SetColorIndex(FOREGROUND_COLOR);
	gc_NoClipHorizLine(0, LARGE_ICON_HEIGHT, LARGE_ICON_WIDTH + 1);
	gc_NoClipVertLine(LARGE_ICON_WIDTH, 0, LARGE_ICON_HEIGHT + 1);
	
	gc_PrintStringXY(entry->listDisplayName, 1, LARGE_ICON_HEIGHT + 2);
	
	type = &menuTag->pokedex->types[entry->type1 - 1];
	gc_NoClipDrawSprite(type->iconData, 1, LARGE_ICON_HEIGHT + 12, type->iconWidth, type->iconHeight);
	if (entry->type2 != 0) {
		type = &menuTag->pokedex->types[entry->type2 - 1];
		gc_NoClipDrawSprite(type->iconData, 37, LARGE_ICON_HEIGHT + 12, type->iconWidth, type->iconHeight);
	}

	word_wrap(entry->description, 140, 0, 0, 1, LARGE_ICON_HEIGHT + 27);

	draw_function_keys(options);
}