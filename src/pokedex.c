/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Shared libraries */
#include <lib\ce\fileioc.h>
#include <lib\ce\graphc.h>

#include "pokedex.h"

PokedexEntry* pokedex_read_next_entry(ti_var_t mainFile) {
    PokedexEntry* entry;

    entry = malloc(sizeof(PokedexEntry));

	if (entry == NULL) {
		dbg_printf(dbgout, "Failed to allocate entry.\n");
		debugger();
	}

	if (ti_Read(&entry->id, 2, 1, mainFile) == 0) {
		free(entry);
		return NULL;
	}

    ti_Read(&entry->isDefault, 1, 1, mainFile);
	ti_Read(&entry->statsAppvarIndex, 1, 1, mainFile);
	ti_Read(&entry->statsLocation, 2, 1, mainFile);

	entry->iconData = NULL;
	entry->name = NULL;
	entry->genus = NULL;
	entry->listDisplayName = NULL;
	entry->description = NULL;
	
    return entry;
}

uint8_t* pokedex_read_image(uint8_t* width, uint8_t* height, ti_var_t file) {
	uint8_t* image;

	ti_Read(width, 1, 1, file);
	ti_Read(height, 1, 1, file);

	image = malloc((*width) * (*height));
	if (image != NULL) {
		ti_Read(image, (*width) * (*height), 1, file);
	} else {
		dbg_printf(dbgout, "Failed to allocate image.\n");
		debugger();
	}

	return image;
}

void pokedex_read_all_pokemon(Pokedex* pokedex, ti_var_t mainFile) {
	PokedexEntry* list;
	PokedexEntry* entry;
	PokedexEntry* nextEntry;
	uint16_t i = 0;
	char* loadingString = malloc(256);

	list = malloc(pokedex->numPokemon * sizeof(PokedexEntry));
	
	entry = NULL;
	gc_SetColorIndex(255);
	gc_PrintStringXY(loadingString, 30, 10);
	while (i < pokedex->numPokemon) {
		nextEntry = pokedex_read_next_entry(mainFile);
		while (!nextEntry->isDefault && entry != NULL) {
			entry->nonDefaultPokemon = nextEntry;
			entry = nextEntry;
			nextEntry = pokedex_read_next_entry(mainFile);
		}
		entry = nextEntry;
		list[i++] = *entry;
	}

	free(loadingString);
	pokedex->pokemon = list;
}

char* pokedex_read_string(ti_var_t file) {
	uint8_t strLen;
	char* string;

	ti_Read(&strLen, 1, 1, file);
	string = malloc(strLen + 1);
	ti_Read(string, strLen, 1, file);
	string[strLen] = 0;
	return string;
}

ti_var_t pokedex_get_data_file(Pokedex* pokedex, PokedexEntry* entry) {
	char* dataAppvarName;
	ti_var_t dataFile = pokedex->dataFile;

	if (entry->statsAppvarIndex != pokedex->currentIndex) {
		pokedex->currentIndex = entry->statsAppvarIndex;
		ti_Close(dataFile);

		dataAppvarName = malloc(8);
		sprintf(dataAppvarName, "[PKDXD%i", entry->statsAppvarIndex);

		dataFile = ti_Open(dataAppvarName, "r");
		free(dataAppvarName);
		pokedex->dataFile = dataFile;
	}

	return dataFile;
}

PokemonStatsData* pokedex_read_stats(Pokedex* pokedex, PokedexEntry* entry) {
	PokemonStatsData* ret = malloc(sizeof(PokemonStatsData));
	ti_var_t dataFile = pokedex_get_data_file(pokedex, entry);

	ti_Seek(entry->statsOffset, 2, dataFile);
	ti_Read(ret, sizeof(PokemonStatsData), 1, dataFile);

	return ret;
}

void pokedex_read_initial_data(Pokedex* pokedex, PokedexEntry* entry) {
	ti_var_t dataFile = pokedex_get_data_file(pokedex, entry);	

	ti_Seek(entry->statsLocation, 2, dataFile);
	
	entry->name = pokedex_read_string(dataFile);
	entry->genus = pokedex_read_string(dataFile);

	entry->iconData = pokedex_read_image(&entry->iconWidth, &entry->iconHeight, dataFile);

	entry->listDisplayName = malloc(7 + strlen(entry->name));
	sprintf(entry->listDisplayName, "%i - %s", entry->id, entry->name);

	entry->description = pokedex_read_string(dataFile);
	
	ti_Read(&entry->type1, 1, 1, dataFile);
	ti_Read(&entry->type2, 1, 1, dataFile);

	entry->statsOffset = ti_Tell(dataFile);
}

void pokedex_delete_initial_data(PokedexEntry* entry) {
    if (entry == NULL) { return; }
    
	if (entry->iconData != NULL) { free(entry->iconData); }
	if (entry->name != NULL) { free(entry->name); }
	if (entry->genus != NULL) { free(entry->genus); }
	if (entry->listDisplayName != NULL) { free(entry->listDisplayName); }
	if (entry->description != NULL) { free(entry->description); }

    entry->iconData = NULL;
	entry->name = NULL;
	entry->genus = NULL;
	entry->listDisplayName = NULL;
	entry->description = NULL;
}

void pokedex_delete_entry(PokedexEntry* entry) {
    if (entry == NULL) { return; }

	pokedex_delete_initial_data(entry);

    free(entry);
}

char* pokedex_read_ability_name(Pokedex* pokedex, uint16_t id) {
	uint16_t readId;
	ti_var_t mainFile = pokedex->mainFile;
	
	ti_Seek(pokedex->abilityDataLocation, 2, pokedex->mainFile);
	while (true) {
		if (!ti_Read(&readId, 2, 1, mainFile)) { break; }
		if (readId == id) {
			return pokedex_read_string(mainFile);
		} else {
			ti_Read(&readId, 1, 1, mainFile);
			ti_Seek(readId, 0, mainFile);
		}
	}

	return NULL;
}

void pokedex_read_types(Pokedex* pokedex, ti_var_t typeFile) {
	uint8_t i;
	PokemonType* currentType;
	
	ti_Read(&pokedex->numTypes, 1, 1, typeFile);
	pokedex->types = malloc(pokedex->numTypes * sizeof(PokemonType));
	
	for (i = 0; i < pokedex->numTypes; i++) {
		currentType = &(pokedex->types[i]);
		ti_Read(&currentType->id, 1, 1, typeFile);
		currentType->name = pokedex_read_string(typeFile);
		currentType->iconData = pokedex_read_image(&currentType->iconWidth, &currentType->iconHeight, typeFile);
	}
}

Pokedex* pokedex_start(void* logo_loaded_call_back) {
	ti_var_t typeFile;
	Pokedex* pokedex;
	ti_CloseAll();

	pokedex = malloc(sizeof(Pokedex));

	pokedex->mainFile = ti_Open("[PKDXM", "r");
	pokedex->dataFile = ti_Open("[PKDXD0", "r");

	ti_Read(&pokedex->numPokemon, 2, 1, pokedex->mainFile);
	pokedex->logo = pokedex_read_image(&pokedex->logoWidth, &pokedex->logoHeight, pokedex->mainFile);

	if (logo_loaded_call_back != NULL) {
		void(*func)(uint8_t* logo, uint8_t width, uint8_t height) = logo_loaded_call_back;
		func(pokedex->logo, pokedex->logoWidth, pokedex->logoHeight);
	}

	pokedex->cursor = pokedex_read_image(&pokedex->cursorWidth, &pokedex->cursorHeight, pokedex->mainFile);

	pokedex_read_all_pokemon(pokedex, pokedex->mainFile);
	pokedex_read_types(pokedex, pokedex->mainFile);
	pokedex->abilityDataLocation = ti_Tell(pokedex->mainFile);
	
	return pokedex;
}

void pokedex_close(Pokedex* pokedex) {
	uint16_t i = 0;
    ti_Close(pokedex->dataFile);
	ti_Close(pokedex->mainFile);

	for (i = 0; i < pokedex->numPokemon; i++) {
		pokedex_delete_initial_data(&pokedex->pokemon[i]);
	}
	
	for (i = 0; i < pokedex->numTypes; i++) {
		free(pokedex->types[i].iconData);
		free(pokedex->types[i].name);
	}

	free(pokedex->pokemon);
	free(pokedex->types);
	free(pokedex->logo);
	free(pokedex->cursor);
	free(pokedex);
}