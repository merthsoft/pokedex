#ifndef MAIN_H
#define MAIN_H

/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "menu.h"

#define BACKGROUND_COLOR 255
#define FOREGROUND_COLOR 0

#define LCD_WIDTH_PX 320
#define LCD_HEIGHT_PX 240

#define LARGE_ICON_WIDTH 120
#define LARGE_ICON_HEIGHT 90

#define DISPLAY_SIZE 6

#include "pokedex.h"

typedef struct MenuTag {
	Pokedex* pokedex;
	uint16_t selectedPokemon;
	uint16_t topPokemon;
} MenuTag;

#define center(max, value) ((max - value) / 2)

void main(void);
Menu* init_menu(Pokedex* pokedex);
void draw_large_icon(MenuEventArgs* menuArgs);
void next_page(MenuEventArgs* menuArgs);
void prev_page(MenuEventArgs* menuArgs);
void load_menu_items(Menu* mainMenu, Pokedex* pokedex, uint16_t top);
void draw_logo_while_loading(uint8_t* logo, uint8_t width, uint8_t height);
void select_pokemon(MenuEventArgs* menuArgs);
void draw_function_keys(char* options[]);

#endif
